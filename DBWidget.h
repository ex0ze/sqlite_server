#ifndef DBWIDGET_H
#define DBWIDGET_H

#include <QWidget>
#include <QList>
#include <QMap>
#include <QString>
#include <QTimer>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QListWidget>
#include <QListWidgetItem>
#include <QRegularExpression>
#include "DBAsync.h"

struct TableData {
    QString tname;
    QList<QString> theaders;
    QList<QList<QString>> tdata;
};

namespace Ui {
class DBWidget;
}

class DBWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DBWidget(QWidget *parent = nullptr);
    void closeEvent(QCloseEvent *event) override;
    ~DBWidget();
public slots:
    void setUpdateInterval(int ms);
    void attachDB(DBAsync*);
signals:
    void shouldClose();
private:
    Ui::DBWidget *ui;
    int m_updateInterval;
    DBAsync* db;
    QTimer* m_updater;
    QList<TableData> m_tables;
//    QMap<int,> m_tablesData;
    void fillTables();
    void fillTable(QListWidgetItem*);
    const QString m_getTablesQuery = "SELECT name FROM sqlite_master WHERE type = 'table' AND name NOT LIKE 'sqlite_%';";
    const QString m_getTablesRegex = "name=([a-zA-Z0-9 -_]*)";
};

#endif // DBWIDGET_H
