#include "QueryCrafter.h"
#include "ui_QueryCrafter.h"

QueryCrafter::QueryCrafter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QueryCrafter)
{
    ui->setupUi(this);
    setWindowIcon(QIcon(QString(":/ico/lightning.ico")));
}

QueryCrafter::~QueryCrafter()
{
    delete ui;
}

void QueryCrafter::on_clearResponseBtn_clicked()
{
    ui->responseEdit->clear();
}

void QueryCrafter::on_executeBtn_clicked()
{
    emit executeThis(ui->queryEdit->toPlainText());
}

void QueryCrafter::on_closeBtn_clicked()
{
    this->close();
}

void QueryCrafter::setDefaultQuery(const QString &query)
{
    ui->queryEdit->setPlainText(query);
}

void QueryCrafter::setResponse(const QString &response)
{
    ui->responseEdit->setPlainText(response);
}

void QueryCrafter::closeEvent(QCloseEvent *event)
{
    emit shouldClose();
    event->accept();
}
