#ifndef CREATEDBDIALOG_H
#define CREATEDBDIALOG_H

#include <QDialog>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QListWidget>
#include <QListWidgetItem>
#include <QFileDialog>
#include <QRegularExpression>
#include <QInputDialog>
#include <QLineEdit>
#include <QPushButton>
#include <QMenu>
#include <QCheckBox>
#include <QMessageBox>
#include <QString>
#include <QList>

class DBColumn_local {
public:
    DBColumn_local();
    enum ColType {
        NONE = 0,
        INTEGER,
        REAL,
        TEXT,
        NUMERIC
    };
    QString m_name;
    bool notNull;
    bool primaryKey;
    bool autoIncrement;
    ColType type;
};

typedef QPair<QString,QList<DBColumn_local>> TableRecord;

namespace Ui {
class CreateDbDialog;
}

class CreateDbDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateDbDialog(QWidget *parent = nullptr);
    void extractData(QString& query, QString& dbPath);
    void closeEvent(QCloseEvent *) override;
    ~CreateDbDialog();

private slots:
    void on_dbPathBtn_clicked();

    void on_newTableBtn_clicked();

    void on_delTableBtn_clicked();

    void on_newColBtn_clicked();

    void on_delColBtn_clicked();

    void updateList();

    void updateTable(QListWidgetItem*);

    void setTableHeader();

    void checkboxPrimaryKeyChanged(int newState);

    void updateRecords(QTableWidgetItem*);

    void on_moveUpBtn_clicked();

    void on_moveDownBtn_clicked();

    void on_acceptBtn_clicked();

    void on_cancelBtn_clicked();

private:
    Ui::CreateDbDialog *ui;
    QString m_dbPath;
    QList<TableRecord> m_tables;
};

#endif // CREATEDBDIALOG_H
