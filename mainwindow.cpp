#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowIcon(QIcon(":/ico/xyz.png"));
    connect(ui->menuDB->actions()[0], &QAction::triggered, this, &MainWindow::showCreateDbDialog);
    connect(ui->menuDB->actions()[1], &QAction::triggered, this, &MainWindow::showOpenDbDialog);
    connect(ui->menuQuery->actions()[0], &QAction::triggered, this, &MainWindow::showCrafter);
    connect(ui->menuServer->actions()[0], &QAction::triggered, this, &MainWindow::onStartServer);
    connect(ui->menuServer->actions()[1], &QAction::triggered, this, &MainWindow::onStopServer);
    db = nullptr;
    dbWidget = nullptr;
    dbWidget_subw = nullptr;
    crafter = nullptr;
    crafter_subw = nullptr;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showCreateDbDialog()
{
    CreateDbDialog dlg;
    if (dlg.exec() == QDialog::Rejected) return;
    QString query_apr, dbpath;
    dlg.extractData(query_apr,dbpath);
    if (db != nullptr) {
        db->closeDB();
        delete db;
    }
    db = new DBAsync(this);
    connect(db, &DBAsync::dbOpened, [&](){
        ui->menuQuery->actions()[0]->setEnabled(true);
    });
    connect(db, &DBAsync::dbClosed, [&](){
        ui->menuQuery->actions()[0]->setEnabled(false);
    });
    connect(db, &DBAsync::sendLog,[&](const QString& log){
       ui->logEdit->append(log);
    });
    db->openDB(dbpath);
    if (crafter != nullptr) {
        delete crafter;
    }
    crafter = new QueryCrafter();
    crafter->setDefaultQuery(query_apr);
    connect(crafter,&QueryCrafter::executeThis,db,&DBAsync::localExecute);
    connect(db,&DBAsync::sendLocalResponse,crafter,&QueryCrafter::setResponse);
    crafter_subw = ui->mdiArea->addSubWindow(crafter);
    crafter->show();
    ui->menuServer->actions()[0]->setEnabled(true);
    if (dbWidget != nullptr) {
        delete dbWidget;
        dbWidget_subw->close();
    }
    dbWidget = new DBWidget();
    dbWidget_subw = ui->mdiArea->addSubWindow(dbWidget);
    connect(dbWidget,&DBWidget::shouldClose,[&](){
        dbWidget_subw->close();
        delete dbWidget;
        dbWidget = nullptr;
        dbWidget_subw = nullptr;
    });
    dbWidget->show();
    dbWidget->attachDB(db);

}

void MainWindow::showOpenDbDialog()
{
    QFileDialog dlg;
    dlg.setFileMode(QFileDialog::FileMode::AnyFile);
    QString fname = dlg.getOpenFileName(nullptr, QString("Select existing db"),QDir::currentPath(), QString("DB (*.db)"));
    if (fname.isEmpty()) return;
    if (db != nullptr) {
        db->closeDB();
        delete db;
    }
    db = new DBAsync(this);
    connect(db, &DBAsync::dbOpened, [&](){
        ui->menuQuery->actions()[0]->setEnabled(true);
    });
    connect(db, &DBAsync::dbClosed, [&](){
        ui->menuQuery->actions()[0]->setEnabled(false);
    });
    connect(db, &DBAsync::sendLog,[&](const QString& log){
       ui->logEdit->append(log + QString('\n'));
    });
    if (!db->openDB(fname)) {
        QMessageBox::critical(nullptr, QString("Open error"), QString("Can't open database"));
        delete db;
        db = nullptr;
    }
    ui->menuServer->actions()[0]->setEnabled(true);
    if (dbWidget != nullptr) {
        delete dbWidget;
        dbWidget_subw->close();
    }
    dbWidget = new DBWidget();
    dbWidget_subw = ui->mdiArea->addSubWindow(dbWidget);
    connect(dbWidget,&DBWidget::shouldClose,[&](){
        dbWidget_subw->close();
        delete dbWidget;
        dbWidget = nullptr;
        dbWidget_subw = nullptr;
    });
    dbWidget->show();
    dbWidget->attachDB(db);
}

void MainWindow::showCrafter()
{
    if (crafter != nullptr) {
        delete crafter;
        crafter_subw->close();
    }
    crafter = new QueryCrafter();
    connect(crafter,&QueryCrafter::executeThis,db,&DBAsync::localExecute);
    connect(db,&DBAsync::sendLocalResponse,crafter,&QueryCrafter::setResponse);
    crafter_subw = ui->mdiArea->addSubWindow(crafter);
    connect(crafter,&QueryCrafter::shouldClose,[&](){
        crafter_subw->close();
        delete crafter;
        crafter = nullptr;
        crafter_subw = nullptr;
    });
    crafter->show();
}

void MainWindow::onStartServer()
{
    bool ok;
    int port = QInputDialog::getInt(nullptr,QString("Server port"),QString("Enter port, on which server will start"),5678,1,65535,1,&ok);
    if (!ok) {
        QMessageBox::critical(nullptr,QString("Port error"),QString("Port must be between 1 and 65535"));
        return;
    }
    db->setPort(port);
    if (!db->run()) return;
    ui->menuServer->actions()[0]->setEnabled(false);
    ui->menuServer->actions()[1]->setEnabled(true);
}

void MainWindow::onStopServer()
{
    db->stop();
    ui->menuServer->actions()[0]->setEnabled(true);
    ui->menuServer->actions()[1]->setEnabled(false);
}
